"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
// import { path } from 'path';
var win;
function createWindow() {
    win = new electron_1.BrowserWindow({ width: 600, height: 800 });
    // let indexPath = path.join(__dirname, "index.html");
    win.loadURL("file://" + __dirname + "/index.html");
    // win.loadFile(indexPath)
    win.webContents.openDevTools();
    win.on("closed", function () {
        win = null;
    });
}
electron_1.app.on('ready', createWindow);
electron_1.app.on("window-all-closed", function () {
    if (process.platform !== "darwin") {
        electron_1.app.quit();
    }
});
