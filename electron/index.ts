import { app, BrowserWindow } from 'electron';
// import { path } from 'path';

let win;

function createWindow() {
    win = new BrowserWindow({ width: 600, height: 800 })


    // let indexPath = path.join(__dirname, "index.html");
    win.loadURL(`file://${__dirname}/index.html`);
    // win.loadFile(indexPath)

    win.webContents.openDevTools();

    win.on("closed", () => {
        win = null;
    })
}

app.on('ready', createWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin"){
        app.quit();
    }
})

